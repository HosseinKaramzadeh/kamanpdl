import tensorflow as tf
import numpy as np
import IPython.display as display
import os
val_dataset_dirc = os.path.abspath('../../tf_records/test.record')


train_record_file =  os.path.abspath('../../tf_records/train.record')
#
def _parse_image_function(example_proto):
  # Parse the input tf.Example proto using the dictionary above.
  return tf.io.parse_single_example(example_proto, feature_description)


feature_description = {
    'image/filename': tf.io.FixedLenFeature([], tf.string),
    'image/encoded': tf.io.FixedLenFeature([], tf.string),
    'image/object/bbox/xmin': tf.io.VarLenFeature(tf.float32),
    'image/object/bbox/ymin': tf.io.VarLenFeature(tf.float32),
    'image/object/bbox/xmax': tf.io.VarLenFeature(tf.float32),
    'image/object/bbox/ymax': tf.io.VarLenFeature(tf.float32),
    'image/object/class/label': tf.io.VarLenFeature(tf.int64),
}


#raw_train_dataset = tf.data.TFRecordDataset(val_dataset_dirc)
#parsed_image_dataset = raw_train_dataset.map(_parse_image_function)
#for x in parsed_image_dataset:
#    tf.print(x['image/object/bbox/xmin'])
#    break


def _parse_tfrecord(x):  
    x_train = tf.image.decode_jpeg(x['image/encoded'], channels=3)
    x_train = tf.image.resize(x_train, (416, 416))    
    labels = tf.cast(1, tf.float32)
#    print(type(x['image/object/bbox/xmin']))
    tf.print(x['image/object/bbox/xmin'])

    y_train = tf.stack([tf.sparse.to_dense(x['image/object/bbox/xmin']),
                        tf.sparse.to_dense(x['image/object/bbox/ymin']),
                        tf.sparse.to_dense(x['image/object/bbox/xmax']),
                        tf.sparse.to_dense(x['image/object/bbox/ymax']),
                        labels], axis=1)

    paddings = [[0, 100 - tf.shape(y_train)[0]], [0, 0]]
    y_train = tf.pad(y_train, paddings)
    return x_train, y_train


def load_tfrecord_dataset(train_record_file, size=416):

    dataset=tf.data.TFRecordDataset(train_record_file)
    parsed_dataset = dataset.map(_parse_image_function)
    final = parsed_dataset.map(_parse_tfrecord)
    return final


load_tfrecord_dataset(train_record_file,416)
#SparseTensor(indices=Tensor("DeserializeSparse_1:0", shape=(None, 1), dtype=int64), values=Tensor("DeserializeSparse_1:1", shape=(None,), dtype=float32),