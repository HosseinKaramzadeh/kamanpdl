#Note : run this code in mother folder or change image_path in main() function
import os
import glob
import pandas as pd
import xml.etree.ElementTree as ET
import math


def xml_to_csv(path):
    print(path)
    xml_list = []
    
#    for xml_file in glob.glob(path + '/*.xml'):
    for xml_file in glob.glob(path + '/*.xml'):
#        print(xml_file)
        tree = ET.parse(xml_file)
        root = tree.getroot()
        var_index = []
        A = root.findall('object')
        #first finding index of bndbox which are for coordination of plate itself not numbers in it
        for member in A:
            B = member.findall('item')
            for m in B:
                s= m.findall('name')
                if s[0].text == 'Plate':
#                    print('plate')
#                    print(list(A).index(member))
                    var_index.append(list(B).index(m))
                    
#                    print(list(s).index(m))
        for member in root.findall('object'):
            for i in var_index:
                dd= root.find('filename').text
                dd=dd.replace("xml","jpg")
                value = (dd,
                         int(root.find('size')[0].text),
                         int(root.find('size')[1].text),
                         member[i][0].text,
                         int(math.floor(float(member[i][4][0].text))),
                         int(math.floor(float(member[i][4][1].text))),
                         int(math.floor(float(member[i][4][2].text))),
                         int(math.floor(float(member[i][4][3].text)))
                         )
                xml_list.append(value)            

    column_name = ['filename', 'width', 'height', 'class', 'xmin', 'ymin', 'xmax', 'ymax']
    xml_df = pd.DataFrame(xml_list, columns=column_name)
    return xml_df



image_path = os.path.abspath('../annotations/xlms/train')
xml_df = xml_to_csv(image_path)
xml_df.to_csv('train_labels.csv', index=None)
print('Successfully converted xml to csv')


