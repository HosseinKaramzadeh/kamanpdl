#Note : run this code in mother folder or change image_path in main() function
import os
import glob
import pandas as pd
import xml.etree.ElementTree as ET
import math

def xml_to_csv(path):
    print(path)
    xml_list = []
    for xml_file in glob.glob(path + '/*.xml'):
        tree = ET.parse(xml_file)
        root = tree.getroot()
        for member in root.findall('object'):
            for i in [0,9,18,27,36,45,54]:
                print(member[i][0].text)
                value = (root.find('filename').text,
                         int(root.find('size')[0].text),
                         int(root.find('size')[1].text),
                         member[i][0].text,
                         int(math.floor(float(member[i][4][0].text))),
                         int(math.floor(float(member[i][4][1].text))),
                         int(math.floor(float(member[i][4][2].text))),
                         int(math.floor(float(member[i][4][3].text)))
                         )
                xml_list.append(value)
    column_name = ['filename', 'width', 'height', 'class', 'xmin', 'ymin', 'xmax', 'ymax']
    xml_df = pd.DataFrame(xml_list, columns=column_name)
    return xml_df


def main():
    image_path = os.path.join(os.getcwd(), 'annotations/xlms/test')
    xml_df = xml_to_csv(image_path)
    xml_df.to_csv('raccoon_labels.csv', index=None)
    print('Successfully converted xml to csv.')


main()
